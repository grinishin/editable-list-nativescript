import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";

import {AppComponent} from "./app.component";
import {ListModule} from "./list/list.module";

import 'nativescript-localstorage';

@NgModule({
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        ListModule
    ],
    schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {
}
