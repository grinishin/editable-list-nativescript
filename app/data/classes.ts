/**
 * Created by grinishin on 20.11.2017.
 */

export class ListItem {
    name: string;
    date: string;
    value: number;
    weekCount: number;
    id: number;
    dateInDate: string;

    constructor(
        name: string,
        date: string,
        value: number,
        weekCount: number,
        id: number
    ){
        this.name = name;
        this.value = value;
        this.weekCount = weekCount;
        this.id = id;

        let newDate = (new Date(date)).toLocaleString().split(' ');
        this.date = `${newDate[1]} ${newDate[2]}, ${newDate[3]}`;
        this.dateInDate = date;
    }
}