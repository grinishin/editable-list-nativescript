/**
 * Created by grinishin on 20.11.2017.
 */

const DATA = `
    [
    
    {
        "name": "Azeroth", "date": "2017-10-23", "day": 193, "week_count": 1029, "month_count": 5355, "week_rate": -13.383838383838, "month_rate": 50.336889387984, "place_day": 1, "place_shift_day": 2, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Aldburg", "date": "2017-10-24", "day": 157, "week_count": 1061, "month_count": 5457, "week_rate": -1.6682113067655, "month_rate": 49.057634526086, "place_day": 1, "place_shift_day": 0, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    
    ,
    {
        "name": "Esgaroth", "date": "2017-10-26", "day": 294, "week_count": 1288, "month_count": 5710, "week_rate": 40.919037199125, "month_rate": 49.281045751634, "place_day": 1, "place_shift_day": 0, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Forlond", "date": "2017-10-27", "day": 312, "week_count": 1407, "month_count": 5915, "week_rate": 55.986696230599, "month_rate": 48.879939592248, "place_day": 1, "place_shift_day": 0, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Rivendell", "date": "2017-10-28", "day": 216, "week_count": 1503, "month_count": 5878, "week_rate": 66.261061946903, "month_rate": 38.468786808009, "place_day": 2, "place_shift_day": -1, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Pelargir", "date": "2017-10-29", "day": 91, "week_count": 1533, "month_count": 5586, "week_rate": 48.979591836735, "month_rate": 30.819672131148, "place_day": 2, "place_shift_day": 0, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Gondolin", "date": "2017-10-30", "day": 166, "week_count": 1506, "month_count": 5622, "week_rate": 41.941564561734, "month_rate": 32.90780141844, "place_day": 1, "place_shift_day": 1, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Harlond", "date": "2017-10-31", "day": 321, "week_count": 1670, "month_count": 5886, "week_rate": 45.09122502172, "month_rate": 33.711949114039, "place_day": 1, "place_shift_day": 0, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Umbar", "date": "2017-11-01", "day": 286, "week_count": 1686, "month_count": 5961, "week_rate": 30.900621118012, "month_rate": 28.248709122203, "place_day": 2, "place_shift_day": -1, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Valmar", "date": "2017-11-02", "day": 182, "week_count": 1574, "month_count": 5897, "week_rate": 11.869225302061, "month_rate": 24.016824395373, "place_day": 1, "place_shift_day": 1, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Nogrod", "date": "2017-11-03", "day": 265, "week_count": 1527, "month_count": 5931, "week_rate": 1.5968063872255, "month_rate": 21.213979153893, "place_day": 1, "place_shift_day": 0, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ,
    {
        "name": "Ondosto", "date": "2017-11-04", "day": 132, "week_count": 1443, "month_count": 5806, "week_rate": -5.8708414872798, "month_rate": 14.134067230195, "place_day": 2, "place_shift_day": -1, "place_week": 1, "place_shift_week": 0, "place_month": 1, "place_shift_month": 0
    }
    
    ]
`;

export function getData(){
    localStorage.setItem('data', DATA);
    let data = localStorage.getItem('data');
    if (data){
        return data;
    } else {
        return DATA;
    }
}