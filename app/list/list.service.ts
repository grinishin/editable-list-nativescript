/**
 * Created by grinishin on 20.11.2017.
 */

import {Injectable} from '@angular/core';

import {getData} from '../data/data';
import {ListItem} from '../data/classes';


@Injectable()
export class ListService {
    getList(): ListItem[] {
        let res = JSON.parse(getData());
        return res.map((elem: any, index: number) => {
            return new ListItem(
                elem.name,
                elem.date,
                elem.day,
                elem.week_count,
                index
            );
        });
    }
}

@Injectable()
export class ListItemService {
    public item: ListItem;

    push(item: ListItem){
        this.item = item;
    }

}
