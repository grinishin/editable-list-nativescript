/**
 * Created by grinishin on 20.11.2017.
 */
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ListComponent} from "./list.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";
import {ItemDetailsComponent} from "./item-details/item-details.component";
import {ListItemService} from "./list.service";
import {EditorComponent} from "./item-details/editor/editor.component";

export const routes = [
    { path: "", redirectTo: "/list", pathMatch: "full" },
    { path: "list", component: ListComponent },
    { path: "list/details/:id", component: ItemDetailsComponent },
    { path: "list/details/:id/edit", component: EditorComponent }
];

@NgModule({
    declarations: [
        ListComponent,
        ItemDetailsComponent,
        EditorComponent
    ],
    // bootstrap: [ListComponent],
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(routes)
    ],
    exports: [NativeScriptRouterModule],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [ListItemService]
})
export class ListModule {}