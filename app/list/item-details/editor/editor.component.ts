/**
 * Created by grinishin on 21.11.2017.
 */

import {Component, OnInit} from "@angular/core";
import {RouterExtensions} from "nativescript-angular";
import {Page} from "tns-core-modules/ui/page";
import {ListItemService} from "../../list.service";
import {ListItem} from "../../../data/classes";
import {DatePicker} from "tns-core-modules/ui/date-picker";

@Component({
    templateUrl: './list/item-details/editor/editor.component.html'
})
export class EditorComponent implements OnInit {

    item: ListItem;

    constructor(
        private routerExtension: RouterExtensions,
        private _page: Page,
        private listItemService: ListItemService
    ){
        _page.actionBarHidden = true;
    }

    ngOnInit(){
        this.item = this.listItemService.item;
        console.dir(this.item)
    }

    back(){
        this.routerExtension.back();
    }

    onTextChange(event){
        console.dir(event.value);
    }

    save(){
        // this.routerExtension.back();
    }

    onPickerLoaded(args) {
        let datePicker = <DatePicker>args.object;
        let date = this.item.dateInDate.split('-');

        datePicker.year = +date[0];
        datePicker.month = +date[1];
        datePicker.day = +date[2];
        datePicker.minDate = new Date(0, 0, 1);
        datePicker.maxDate = new Date();
    }

}