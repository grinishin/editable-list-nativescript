/**
 * Created by grinishin on 20.11.2017.
 */

import {Component, OnInit} from "@angular/core";
import {RouterExtensions} from "nativescript-angular";
import {Page} from "tns-core-modules/ui/page";
import {ListItemService} from "../list.service";
import {ListItem} from "../../data/classes";

@Component({
    templateUrl: './list/item-details/item-details.component.html',
})
export class ItemDetailsComponent implements OnInit{
    item: ListItem;

    constructor(
        private routerExtension: RouterExtensions,
        private _page: Page,
        private listItemService: ListItemService
    ){
        _page.actionBarHidden = true;
    }

    ngOnInit(){
        this.item = this.listItemService.item;
    }

    back(){
        this.routerExtension.back();
    }
}