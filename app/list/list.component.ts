/**
 * Created by grinishin on 20.11.2017.
 */
import {Component, OnInit} from "@angular/core";
import {ListService, ListItemService} from './list.service';
import {ListItem} from '../data/classes';
import {Router} from "@angular/router";

@Component({
    templateUrl: './list/list.component.html',
    providers: [ListService]
})
export class ListComponent implements OnInit{
    private list: ListItem[];

    constructor(
        private listService: ListService,
        private listItemService: ListItemService,
        private router: Router
    ){

    }

    ngOnInit(){
        this.getList();
    }

    getList(){
        this.list = this.listService.getList();
    }

    onItemTap(event){
        this.listItemService.push(this.list[event.index]);
        this.router.navigate([`list/details/${event.index}`]);
    }

}